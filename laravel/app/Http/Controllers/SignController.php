<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SignController extends Controller
{
    public function signUp(Request $request)
    {
        $data = $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);
        $data['password'] = Hash::make($data['password']);
        User::create($data);
        return response(null, 201);
    }

    public function signIn(Request $request)
    {
        $data = $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);
        $user = User::where('email', $data['email'])->first();
        if(!Hash::check($data['password'], $user->password)) {
            return response(null, 401);
        }
        $token = $user->createToken('test');
        return response($token, 200);
    }
}
