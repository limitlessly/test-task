<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function getAuthCode(Request $request)
    {
        try {
            $this->validate($request, [
                'client_id' => 'required|exists:oauth_access_tokens,client_id',
                'redirect_uri' => 'required|exists:oauth_clients,redirect',
                'responseType' => 'required',
                'scope' => 'required'
            ]);
            $codeAuth = Auth::user()->getKey();
            $redirect_uri = $request->query('redirect_uri').'/?auth_code='.$codeAuth;
            return redirect($redirect_uri);
        }catch (\Exception $e) {
            return response([
                'error' => $e->getCode(),
                'message' => $e->getMessage()
            ],403);
        }

    }
    public function callback(Request $request) {
        try {
            $this->validate($request, [
                'auth_code' => 'required',
            ]);
            return response([
                'auth_code' => $request->query('auth_code')
            ], 200);
        }catch (\Exception $e) {
            return response([
                'error' => $e->getCode(),
                'message' => $e->getMessage()
            ]);
        }
    }
    public function getToken(Request $request) {
        try {
            $this->validate($request, [
                'client_id' => 'required|exists:oauth_clients,_id',
                'client_secret' => 'required|exists:oauth_clients,secret',
                'grant_type' => 'required',
                'code' => 'required',
                'redirect_uri' => 'required|exists:oauth_clients,redirect'
            ]);
            if ($request->query('code') !== Auth::user()->getKey()) {
                throw new \Exception('Неверный код авторизации');
            }
            $user = User::find(Auth::id());
            $token = $user->createToken('oauth2');
            return response([
                'accessToken' => $token->accessToken,
                'token_type' => 'bearer',
                'info' => $user
            ], 200);
        }catch (\Exception $e) {
            return $e;
        }
    }
}
