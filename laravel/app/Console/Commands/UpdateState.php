<?php

namespace App\Console\Commands;

use App\DeviceState;
use Illuminate\Console\Command;

class UpdateState extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:state';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $all = DeviceState::all();
        $all->each(function ($item) {
            $device =  DeviceState::find($item->id);
            $device->state = !$item->state;
            $device->save();
        });
    }
}
