<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->prefix('user/device')->group(function() {
    Route::get('get','DeviceController@index');
    Route::post('add', 'DeviceController@store');
    Route::get('state/{device_id}/get', 'DeviceController@showState');
    Route::post('state/{device_id}/set', 'DeviceController@storeSate');
});

Route::post('/auth/signup', 'SignController@signup');
Route::post('/auth/signin', 'SignController@signin');

Route::middleware('auth:api')->prefix('v1')->group(function () {
    Route::get('oauth/authorize', 'AuthController@getAuthCode');
    Route::get('oauth/token', 'AuthController@getToken');
});
